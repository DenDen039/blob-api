package service

import (
	"blob_api/internal/config"
	"blob_api/internal/data/pg"
	"blob_api/internal/service/handlers"

	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
)

func (s *service) router(cfg config.Config) chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxBlobsQ(pg.NewBlobsQ(cfg.DB())),
		),
	)
	r.Route("/integrations/blobs", func(r chi.Router) {
		r.Get("/", handlers.GetBlobs)
		r.Post("/", handlers.CreateBlob)
		r.Route("/{id}", func(r chi.Router) {
			r.Get("/", handlers.GetBlobByID)
			r.Delete("/", handlers.DeleteBlob)
		})
	})

	return r
}
