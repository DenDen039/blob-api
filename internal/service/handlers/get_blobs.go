package handlers

import (
	"blob_api/internal/data"
	"net/http"

	"blob_api/internal/service/requests"
	"blob_api/resources"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func newBlobsList(blobs []data.Blob) []resources.Blob {
	result := make([]resources.Blob, len(blobs))
	for i, blob := range blobs {
		result[i] = newBlobModel(blob)
	}
	return result
}
func GetBlobs(w http.ResponseWriter, r *http.Request) {
	req, err := requests.NewGetBLobsListRequest(r)
	if err != nil {
		Log(r).WithError(err).Error("Incorrect request")
		ape.Render(w, problems.InternalError())
		return
	}
	q := BlobsQ(r)
	ApplyFilter(q, req)
	blobs, err := q.Select()
	if err != nil {
		Log(r).WithError(err).Error("Failed to get blobs")
		ape.Render(w, problems.InternalError())
		return
	}
	// TODO: Join instead of two selects
	result := resources.BlobListResponse{
		Data: newBlobsList(blobs),
	}
	ape.Render(w, result)
}
func ApplyFilter(q data.BlobsQ, request requests.GetBLobsListRequest) {
	q.Page(request.OffsetPageParams)

	if len(request.FilterOwnerID) > 0 {
		q.FilterByOwnerID(request.FilterOwnerID...)
	}

}

// func GetBlobsByOwnerID(w http.ResponseWriter, r *http.Request) {

// 	req, err := requests.NewGetBlobOwnerIDRequest(r)
// 	if err != nil {
// 		Log(r).WithError(err).Error("failed to parse request")
// 		ape.Render(w, problems.InternalError())
// 		return
// 	}
// 	blobs, err := BlobsQ(r).FilterByOwnerID(req.OwnerID).Select()
// 	if err != nil {
// 		Log(r).WithError(err).Error("failed to get blobs from DB")
// 		ape.Render(w, problems.InternalError())
// 		return
// 	}
// 	result := resources.BlobListResponse{
// 		Data: newBlobsList(blobs),
// 	}
// 	ape.Render(w, result)
// }
