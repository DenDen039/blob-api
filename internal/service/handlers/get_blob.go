package handlers

import (
	"blob_api/internal/service/requests"
	"blob_api/resources"
	"net/http"

	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
)

func GetBlobByID(w http.ResponseWriter, r *http.Request) {

	req, err := requests.NewGetBlobIDRequest(r)
	if err != nil {
		Log(r).WithError(err).Error("failed to parse request")
		ape.Render(w, problems.InternalError())
		return
	}
	blob, err := BlobsQ(r).FilterByID(req.BlobID).Get()
	if err != nil {
		Log(r).WithError(err).Error("failed to get blob from DB")
		ape.Render(w, problems.InternalError())
		return
	}
	if blob == nil {
		ape.Render(w, problems.NotFound())
		return
	}
	result := resources.BlobResponse{
		Data: newBlobModel(*blob),
	}
	ape.Render(w, result)
}
